import { useState, FormEvent } from 'react'

import { useAppSelector } from '../../services/redux/hooks.ts'
import './Profile.css'
import { BoundDevice, RenameDevice, ShowDeviceProcess, UnboundDevice } from '../../services/api/requests/deviceRequests.ts'

function Profile() {
  const isDemo = useAppSelector((state) => state.isDemo.value)
  const userName = useAppSelector((state) => state.userName.value)
  const devices = useAppSelector((state) => state.userDevice.value)
  const processes = useAppSelector((state) => state.deviceProcess.value)
  const [callAddDeviceForm, setCallAddDeviceForm] = useState<boolean>(false)
  const [callEditDeviceForm, setCallEditDeviceForm] = useState<boolean>(false)
  const [deviceName, setDeviceName] = useState<string>('')
  const [deviceToken, setDeviceToken] = useState<string>('')
  const [viewedDeviceName, setViewedDeviceName] = useState<string>('')
  const [viewedDeviceId, setViewedDeviceId] = useState<string>('')

  const handleDeviceNameChange = (e: any) => {
    const value: string = e.target.value.trim()
    setDeviceName(value)
  }

  const handleDeviceTokenChange = (e: any) => {
    const value: string = e.target.value.trim()
    setDeviceToken(value)
  }

  const AddDevice = (e: FormEvent) => {
    e.preventDefault()

    if (deviceToken) {
      BoundDevice(deviceToken)

      setDeviceName('')
      setDeviceToken('')
      setCallAddDeviceForm(false)
    }
  }

  const EditDevice = (e: FormEvent) => {
    e.preventDefault()

    if (deviceName && deviceToken) {
      RenameDevice(deviceToken, deviceName)

      setDeviceName('')
      setDeviceToken('')
      setCallEditDeviceForm(false)
    }
  }

  const CloseAddDeviceForm = (e: FormEvent) => {
    e.preventDefault()
    setDeviceName('')
    setDeviceToken('')
    setCallEditDeviceForm(false)
    setCallAddDeviceForm(false)
  }

  const DeleteDevice = (e: FormEvent, id: string) => {
    e.preventDefault()

    if (confirm("Are you sure?")) UnboundDevice(id)
  }

  const CallEditDeviceForm = (name: string, token: string) => {
    setDeviceName(name)
    setDeviceToken(token)
    setCallEditDeviceForm(true)
  }

  const CallViewDevice = (name: string, token: string, id: string) => {
    ShowDeviceProcess(token)
    setViewedDeviceName(name)
    setViewedDeviceId(id)
  }

  return (
    <div className='profile-container'>

      {
        callAddDeviceForm &&
        <div className='device-form-container'>
          <div className='device-form-wrapper'>
            <button className='device-form-exit-button' onClick={ (e) => CloseAddDeviceForm(e) }></button>
            <div className='device-form-title'>
              Device
            </div>
            <div className='device-form-fields'>
              <div className='device-form-field'>
                <input className='device-form-input' type='text' id='devicToken' onChange={handleDeviceTokenChange} value={deviceToken}></input>
                <label className={ deviceToken ? 'device-form-label device-form-label--fixed' : 'device-form-label' }> Device Token </label> 
              </div>
            </div>
            <button className='device-form-button' onClick={ e => AddDevice(e) }> Add Device </button>
          </div>
        </div>
      }

      {
        callEditDeviceForm &&
        <div className='device-form-container'>
          <div className='device-form-wrapper'>
            <button className='device-form-exit-button' onClick={ (e) => CloseAddDeviceForm(e) }></button>
            <div className='device-form-title'>
              Device
            </div>
            <div className='device-form-fields'>
              <div className='device-form-field'>
                <input className='device-form-input' type='text' id='name' onChange={handleDeviceNameChange} value={deviceName}></input>
                <label className={ deviceName ? 'device-form-label device-form-label--fixed' : 'device-form-label' }> Name </label> 
              </div>
            </div>
            <button className='device-form-button' onClick={ e => EditDevice(e) }> Save Device </button>
          </div>
        </div>
      }

      <div className='profile-container-wrapper'>

        <div className='profile-username-container'>
          <span> User: {userName} </span>
        </div>

        <div className='profile-devices-container'> 
          <div className='profile-devices-container-wrapper'>
            <div className='profile-devices__header'> 
              <span> Devices </span>
              {
                !isDemo &&
                <div>
                  <button className='devices-header-button add-button' onClick={ () => setCallAddDeviceForm(true) }> Add Device </button>
                </div>
              }
            </div>
            <div className='profile-devices__body'>
              <table className='profile-devices-table'>
              <tr>
                <th className='devices-table-name-column'>Name</th>
                <th className='devices-table-token-column'>Token</th>
                <th className='devices-table-action-column'>Actions</th>
              </tr>
              {
                devices.map(device => {
                  if (device.token) {
                    return (
                      <tr key="device.token._id">
                        <td className='devices-table-name-column'>
                          <span>{device.name}</span>
                        </td>
                        <td className='devices-table-token-column'>
                          <span>{device.token.computerToken}</span>
                        </td>
                        <td className='devices-table-action-column'>
                          <button className='device-view-button'>
                            <img src='/eye.svg' alt='view' onClick={ () => CallViewDevice(device.name, device.token.computerToken, device.token._id) }/>
                          </button>
                          {
                            !isDemo &&
                            <>
                              <button className='device-edit-button' onClick={ () => CallEditDeviceForm(device.name, device.token.computerToken) }>
                                <img src='/wrenches.svg' alt='edit'/>
                              </button>
                              <button className='device-delete-button' onClick={ (e) => DeleteDevice(e, device.token._id) }>
                                <img src='/trash.svg' alt='delete'/>
                              </button>
                            </>
                          }
                        </td>
                      </tr>
                )}})
              }
              </table>
            </div>
          </div>
        </div>
        <div className='profile-device-processes-container'>
          <div className='profile-device-processes-container-wrapper'>
            <div className='device-processes__header'>
              <span>{viewedDeviceName}</span>
            </div>
            <div className='device-processes__body'>
              <table>
                <tr>
                  <th className='processes-table-pid-column'>PID</th>
                  <th className='processes-table-name-column'>Name</th>
                  <th className='processes-table-time-column'>Time</th>
                </tr>
                {
                  processes.map(process => {
                    if (viewedDeviceId == process.computer) {
                      const date1 = new Date(process.processStartTime).getTime();
                      const date2 = new Date(process.processEndTime).getTime();
  
                      const seconds = Math.floor(Math.abs(date2 - date1) / 1000) % 60;
                      const minutes = Math.floor(Math.floor(Math.abs(date2 - date1) / 1000) / 60) % 60;
                      const hours = Math.floor(Math.floor(Math.floor(Math.abs(date2 - date1) / 1000) / 60) / 60) % 24;
  
                      return (
                        <tr>
                          <td className='processes-table-pid-column'>{process.processUUID}</td>
                          <td className='processes-table-name-column'>{process.processName}</td>
                          <td className='processes-table-time-column'>{hours}:{minutes}:{seconds}</td>
                        </tr>
                      )
                    }
                })
                }
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Profile