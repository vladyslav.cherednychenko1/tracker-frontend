import { Link, Navigate } from 'react-router-dom'
import { useState, FormEvent } from 'react'

import { useAppSelector } from '../../../services/redux/hooks.ts'
import { AuthDemo, CreateUser } from '../../../services/api/requests/authRequests.ts'
import '../Auth.css'

function SignUp() {
  const errorMessage = useAppSelector((state) => state.errorHandler.value)

  const [emailError, setEmailError] = useState<string>('')
  const [passwordError, setPasswordError] = useState<string>('')

  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [confirmPassword, setConfirmPassword] = useState<string>('')
  const [isRedirect, setRedirect] = useState<boolean>(false)

  const handleEmailChange = (e: any) => {
    const value: string = e.target.value.trim().toLowerCase()
    setEmail(value)
  }

  const handlePasswordChange = (e: any) => {
    const value: string = e.target.value.trim()
    setPassword(value)
  }

  const handleConfirmPasswordChange = (e: any) => {
    const value: string = e.target.value.trim()
    setConfirmPassword(value)
  }

  const SignUpValidation = (e: FormEvent) => {
    e.preventDefault()

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    let isError = false

    if (emailRegex.test(email)) {
      setEmailError('')
    } else {
      setEmailError('Invalid email')
      isError = true
    }

    if (confirmPassword === password) {
      if (password.length >= 8 && password.length <= 32) {
        setPasswordError('')
      } else {
        setPasswordError('Password length must be between 8 and 32 characters')
        isError = true
      }

      if (password.search(/[a-z]/i) < 0) {
        setPasswordError("Your password must contain at least one letter.")
        isError = true
      }
      if (password.search(/[0-9]/) < 0) {
        setPasswordError("Your password must contain at least one digit.");
        isError = true
      }

    } else {
      setPasswordError('Those passwords didn`t match. Try again')
      isError = true
    }

    const user = {
      email: email,
      password: password
    }

    if (!(isError)){
      CreateUser(user)
      setRedirect(true)
    }
  }

  return (
    <div className='auth-container'>

      { isRedirect && <Navigate to='/signin' replace /> }

      <div className='auth-title'>
        Sign Up
      </div>

      <div className='auth-wrapper'>
      <div className='auth-field'>
          <input className={ emailError ? 'auth-input auth-input--error' : 'auth-input' } type='text' id='email' onChange={handleEmailChange} style={ emailError ? {} : {} }></input>
          <label className={ email ? 'auth-label auth-label--fixed' : 'auth-label' }> Email </label> 
        </div>
        <div className='auth-field'>
          <input className={ passwordError ? 'auth-input auth-input--error' : 'auth-input' } type='password' id='password' onChange={handlePasswordChange}></input>
          <label className={ password ? 'auth-label auth-label--fixed' : 'auth-label' }> Password </label> 
        </div>
        <div className='auth-field'>
          <input className={ passwordError ? 'auth-input auth-input--error' : 'auth-input' } type='password' id='confirmPassword' onChange={handleConfirmPasswordChange}></input>
          <label className={ confirmPassword ? 'auth-label auth-label--fixed' : 'auth-label' }> Confirm Password </label> 
        </div>
      </div>

      <button className='auth-button' onClick={ e => SignUpValidation(e) }> Sign Up </button>

      <div className='auth-error-handler'>
        { 
          emailError && 
          <div className='auth-error-container'> 
            <img src = '/alert.svg' alt='Alert SVG'/>
            { emailError } 
          </div> 
        }

        { 
          passwordError && 
          <div className='auth-error-container'> 
            <img src = '/alert.svg' alt='Alert SVG'/>
            { passwordError } 
          </div> 
        }

        {
          errorMessage && 
          <div className='auth-error-container'> 
            <img src = '/alert.svg' alt='Alert SVG'/>
            { errorMessage } 
          </div> 
        }
      </div>

      <div className='auth-method'>
        <div className='auth-method-wrapper'>
          <span> Don`t have an account? </span>
          <div className='auth-method-links'>
            <Link to='/signin' >
              <span> Sign In </span>
            </Link>
            <span> or </span>
            <Link to='' onClick={ () => AuthDemo() } >
              <span> Try Demo </span>
            </Link>
          </div>
        </div>
      </div>

    </div>
  )
}

export default SignUp