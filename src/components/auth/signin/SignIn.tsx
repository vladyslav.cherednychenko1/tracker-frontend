import { Link } from 'react-router-dom'
import { useState, FormEvent } from 'react'

import { useAppSelector } from '../../../services/redux/hooks.ts'
import { AuthUser, AuthDemo } from '../../../services/api/requests/authRequests.ts'
import '../Auth.css'

function SignIn() {
  const errorMessage = useAppSelector((state) => state.errorHandler.value)

  const [emailError, setEmailError] = useState<string>('')
  const [passwordError, setPasswordError] = useState<string>('')

  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  const handleEmailChange = (e: any) => {
    const value: string = e.target.value.trim().toLowerCase()
    setEmail(value)
  }

  const handlePasswordChange = (e: any) => {
    const value: string = e.target.value.trim()
    setPassword(value)
  }

  const SignInValidation = (e: FormEvent) => {
    e.preventDefault()

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    let isError = false

    if (emailRegex.test(email)) {
      setEmailError('')
    } else {
      setEmailError('Invalid email')
      isError = true
    }

    if (password.length >= 8 && password.length <= 32) {
      setPasswordError('')
    } else {
      setPasswordError('Password length must be between 8 and 32 characters')
      isError = true
    }

    const user = {
      email: email,
      password: password
    }

    if (!(isError)){
      AuthUser(user)
    }
  }

  return (
    <div className={'auth-container'}>
      
      <div className='auth-title'>
        Sign In
      </div>

      <div className='auth-wrapper'>
        <div className='auth-field'>
          <input className={ emailError ? 'auth-input auth-input--error' : 'auth-input' } type='text' id='email' onChange={handleEmailChange}></input>
          <label className={ email ? 'auth-label auth-label--fixed' : 'auth-label' }> Email </label> 
        </div>
        <div className='auth-field'>
          <input className={ passwordError ? 'auth-input auth-input--error' : 'auth-input' } type='password' id='password' onChange={handlePasswordChange}></input>
          <label className={ password ? 'auth-label auth-label--fixed' : 'auth-label' }> Password </label> 
        </div>
      </div>

      <button className='auth-button' onClick={ e => SignInValidation(e) }> Sign In </button>

      <div className='auth-error-handler'>
        { 
          emailError && 
          <div className='auth-error-container'> 
            <img src = '/alert.svg' alt='Alert SVG'/>
            { emailError } 
          </div> 
        }

        { 
          passwordError && 
          <div className='auth-error-container'> 
            <img src = '/alert.svg' alt='Alert SVG'/>
            { passwordError } 
          </div> 
        }

        {
          errorMessage && 
          <div className='auth-error-container'> 
            <img src = '/alert.svg' alt='Alert SVG'/>
            { errorMessage } 
          </div> 
        }
      </div>

      

      <div className='auth-method'>
        <div className='auth-method-wrapper'>
          <span> Don`t have an account? </span>
          <div className='auth-method-links'>
            <Link to='/signup' >
              <span> Sign Up </span>
            </Link>
            <span> or </span>
            <Link to='' onClick={ () => AuthDemo() } >
              <span> Try Demo </span>
            </Link>
          </div>
        </div>
      </div>

    </div>
  )
}

export default SignIn