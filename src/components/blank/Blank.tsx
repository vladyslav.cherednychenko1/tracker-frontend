import { Link } from 'react-router-dom'

import './Blank.css'

function Blank() {
  return (
    <div className='blank'>
      <Link to='/signin' >
        <span> Sign In </span>
      </Link>
      <p></p>
      <Link to='/signup' >
        <span> Sign Up </span>
      </Link>
    </div>
  )
}

export default Blank