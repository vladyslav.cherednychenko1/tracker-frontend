import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { decodeToken } from "react-jwt";
import Cookies from 'js-cookie'

import { TokenRefresh } from '../services/api/requests/authRequests.ts'
import { useAppSelector, useAppDispatch } from '../services/redux/hooks.ts'
import { loggedIn, loggedOut } from '../services/redux/slice/isAuthSlice'
import { setDemo, unsetDemo } from '../services/redux/slice/isDemoSlice'


import NavBar from './navbar/NavBar.tsx'
import SignIn from './auth/signin/SignIn.tsx'
import SignUp from './auth/signup/SignUp.tsx'
import Profile from './profile/Profile.tsx';

const demoEmail = 'jolepic598@introace.com'

interface tokenDTO {
  email: string
  exp: number
}

function App() {
  const accessToken = useAppSelector((state) => state.accessToken.value)
  const isAuth = useAppSelector((state) => state.isAuth.value)
  const isDemo = useAppSelector((state) => state.isDemo.value)

  const [isRefreshing, setRefreshing] = useState<boolean>(false)
  const [isLoading, setLoading] = useState<boolean>(true)

  const dispatch = useAppDispatch()

  const loggout = () => {
    dispatch(loggedOut())
    dispatch(unsetDemo())
    setLoading(false)
  }

  const userLogged = () => {
    dispatch(loggedIn())
    dispatch(unsetDemo())
    setLoading(false)
  }

  const demoLogged = () => {
    dispatch(loggedIn())
    dispatch(setDemo())
    setLoading(false)
  }

  const isCorrupted = (token: tokenDTO | null) => {
    if (!token) return true

    if (token.email && token.exp) return false

    return true
  }

  useEffect(() => {
    if (isRefreshing) {
      TokenRefresh()
      setRefreshing(false)
    }
  }, [isRefreshing])

  useEffect(() => { 
    console.log('set loading')
    setLoading(true)
  }, [accessToken, Cookies.get('refreshToken')])

  // loading states initialization
  useEffect(() => {
    // Delete on production
    console.log(''
      + `isLoading: ${isLoading}\n`
      + `isAuth: ${isAuth}\n`
      + `isDemo: ${isDemo}\n`
    )
    // #####################

    if (!isLoading) return

    const refreshToken = Cookies.get('refreshToken')

    if (!refreshToken) {
      // Delete on production
      console.log('' +
        + '[1] - tokens not found\n'
        + `accessToken: ${accessToken}\n`
        + `refreshToken: ${refreshToken}\n`
      )
      // #####################
      return loggout()
    } 

    if (!accessToken) {
      const refreshTokenDTO: tokenDTO | null = decodeToken(refreshToken)
      if (isCorrupted(refreshTokenDTO)) {
        // Delete on production
        console.log(''
          + '[2] - refresh token currupted\n'
          + `refreshToken: ${refreshTokenDTO}\n`
        )
        // #####################
        return loggout()
      } else {
        // Delete on production
        console.log('[3] - token refresh request')
        // #####################

        if (!isRefreshing) {
          setRefreshing(true)
          return setLoading(true)
        }
        return setLoading(false)
      }
    }
    
    const accessTokenDTO: tokenDTO | null = decodeToken(accessToken)
    const refreshTokenDTO: tokenDTO | null = decodeToken(refreshToken)

    if (isCorrupted(accessTokenDTO) || isCorrupted(refreshTokenDTO)) {
      // Delete on production
      console.log(''
        + '[4] - refresh token currupted\n'
        + `refreshToken: ${refreshTokenDTO}\n`
      )
      // #####################
      return loggout()
    }

    if (accessTokenDTO?.email === demoEmail) {
      // Delete on production
      console.log('[5] - demo')
      // #####################
      return demoLogged()
    } else {
      // Delete on production
      console.log('[6] - user')
      // #####################
      return userLogged()
    }
  });

  if (isLoading) return <div className='loading'></div>

  if (isAuth)
  {
    if (isDemo)
      return (
        <BrowserRouter>
          <NavBar />
            <Routes>
              <Route path='/demo' element={ <Profile /> } />
              <Route path='*' element={<Navigate to='/demo' replace />} />
            </Routes>
        </BrowserRouter>
      )
    else
      return (
        <BrowserRouter>
          <NavBar />
            <Routes>
              <Route path='/' element={ <Profile /> } />
              <Route path='*' element={<Navigate to='/' replace />} />
            </Routes>
        </BrowserRouter>
      )
  }
  else 
    return (
      <BrowserRouter>
        <NavBar />
          <Routes>
            <Route path='/signin' element={ <SignIn /> } />
            <Route path='/signup' element={ <SignUp /> } />
            <Route path='*' element={<Navigate to='/signin' replace />} />
          </Routes>
      </BrowserRouter>
    )
}

export default App
