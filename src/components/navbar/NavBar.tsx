import { Link } from 'react-router-dom'
import Cookies from 'js-cookie'
// import { Logout } from '../../services/api/requests/authRequests.ts'

import './NavBar.css'
import { useAppSelector, useAppDispatch } from '../../services/redux/hooks.ts'
import { setAccessToken } from '../../services/redux/slice/accessTokenSlice'
import { loggedOut } from '../../services/redux/slice/isAuthSlice'
import { unsetDemo } from '../../services/redux/slice/isDemoSlice'

function NavBar() {
  const isAuth = useAppSelector((state) => state.isAuth.value)
  const dispatch = useAppDispatch()

  const Logout = () => {
    Cookies.remove('refreshToken')
    dispatch(setAccessToken(''))
    dispatch(loggedOut())
    dispatch(unsetDemo()) 
  }

  return (
    <header>
        <Link className='logo-container' to='/' style={{ color: '#fff' }}>
          <img className='logo-icon' src='/logo.svg' alt='Logo SVG'/> 
          <span> iTracker </span> 
        </Link> 
        { isAuth && <a className='exit-container' href='/exit' onClick={ () => Logout() }><img className='exit-icon' src='/exit.svg' alt='EXIT SVG'/></a>}
    </header>
  )
}

export default NavBar