import { decodeToken } from "react-jwt"
import { store } from '../../redux/store'
import Cookies from 'js-cookie'

import API from '../apiConfig'
import { setAccessToken } from "../../redux/slice/accessTokenSlice"
import { setErrorHandler } from "../../redux/slice/errorHandlerSlice"
import { loggedOut } from "../../redux/slice/isAuthSlice"
import { unsetDemo } from "../../redux/slice/isDemoSlice"
import { setUserName } from "../../redux/slice/userNameSlice"
import { setUserDevice } from "../../redux/slice/userDeviceSlice"

interface userDTO {
  email: string
  password: string
}

// Delete on production
interface tokenExp {
  exp: number
}
// #####################

interface tokenEmail {
  email: string
}

interface tokenDevice {
  computers: [];
}

const demoEmail = 'jolepic598@introace.com'
const demoPassword = 'jolepic598'

export async function CreateUser(data: userDTO) {
  await API.post('/user/registration', {
    email: data.email,
    password: data.password,
  })
  .then(function () {
    alert('' 
      + 'Check your mail! \n' 
      + 'And sign in with your data'
    )

    store.dispatch(setErrorHandler(''))
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload

      store.dispatch(setErrorHandler(payload.message))

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

      alert(errorMsg)
    } catch (e) {
      console.error(error)
    }
  })
}

export async function AuthUser(data: userDTO) {
  await API.post('/user/authorization', {
    email: data.email,
    password: data.password,
  })
  .then(function (response) {
    store.dispatch(setAccessToken(response.data.accessToken))
    API.defaults.headers.common['Authorization'] = `Bearer ${response.data.accessToken}`
    
    const email = decodeToken<tokenEmail>(response.data.accessToken)
    store.dispatch(setUserName(email?.email))

    const devices = decodeToken<tokenDevice>(response.data.accessToken)
    store.dispatch(setUserDevice(devices?.computers))
    
    // Delete on production
    const refresh = decodeToken<tokenExp>(response.data.refreshToken)
    Cookies.set('refreshToken', response.data.refreshToken, { expires: new Date( <number>refresh?.exp * 1000 )})
    // #####################

    store.dispatch(setErrorHandler(''))
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload
      store.dispatch(setErrorHandler(payload.message))

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

    } catch (e) {
      console.error(error)
    } 
  })
}

export async function AuthDemo() {
  await API.post('/user/authorization', {
    email: demoEmail,
    password: demoPassword
  })
  .then(function (response) {
    store.dispatch(setAccessToken(response.data.accessToken))
    API.defaults.headers.common['Authorization'] = `Bearer ${response.data.accessToken}`

    const email = decodeToken<tokenEmail>(response.data.accessToken)
    store.dispatch(setUserName(email?.email))

    const devices = decodeToken<tokenDevice>(response.data.accessToken)
    store.dispatch(setUserDevice(devices?.computers))

    // Delete on production
    const refreshToken = decodeToken<tokenExp>(response.data.refreshToken);
    Cookies.set('refreshToken', response.data.refreshToken, { expires: new Date( <number>refreshToken?.exp * 1000 )})
    // #####################

    store.dispatch(setErrorHandler(''))
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload
      store.dispatch(setErrorHandler(payload.message))

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

    } catch (e) {
      console.error(error)
    } 
  })
}

export async function TokenRefresh() {
  await API.get('/user/token/refresh')
  .then(function (response) {
    store.dispatch(setAccessToken(response.data.accessToken))
    API.defaults.headers.common['Authorization'] = `Bearer ${response.data.accessToken}`

    const email = decodeToken<tokenEmail>(response.data.accessToken)
    store.dispatch(setUserName(email?.email))

    const devices = decodeToken<tokenDevice>(response.data.accessToken)
    store.dispatch(setUserDevice(devices?.computers))
    
    // Delete on production
    const refreshToken = decodeToken<tokenExp>(response.data.refreshToken);
    Cookies.set('refreshToken',response.data.refreshToken, { expires: new Date( <number>refreshToken?.exp * 1000 )})
    // #####################

    store.dispatch(setErrorHandler(''))
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload

      store.dispatch(setErrorHandler(payload.message))

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

    } catch (e) {
      console.error(error)
    }
  })
}

export async function Logout() {
  await API.get('/user/logout')
  .then(function () {
    store.dispatch(setAccessToken(''))
    store.dispatch(setUserDevice(''))
    store.dispatch(setUserName(''))
    store.dispatch(loggedOut())
    store.dispatch(unsetDemo()) 

    // Delete on production
    Cookies.remove('refreshToken')
    // #####################

    store.dispatch(setErrorHandler(''))
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload

      store.dispatch(setErrorHandler(payload.message))

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

    } catch (e) {
      console.error(error)
    }
  })
}
