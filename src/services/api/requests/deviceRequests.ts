import { store } from '../../redux/store'

import API from '../apiConfig'
import { TokenRefresh } from './authRequests'

import { setDeviceProcess } from "../../redux/slice/deviceProcessSlice"

export async function BoundDevice(token: string) {
  await API.post('/computer', {
    computerToken: token,
  })
  .then(function () {
    TokenRefresh()
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

    } catch (e) {
      console.error(error)
    } 
  })
}

export async function UnboundDevice(id: string) {
  await API.delete(`/computer/${id}`)
  .then(function () {
    TokenRefresh()
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

    } catch (e) {
      console.error(error)
    } 
  })
}

export async function RenameDevice(token: string, name: string) {
  await API.put(`/computer/local/${token}`, {
    name: name
  })
  .then(function () {
    console.log(`rename device: ${token}`)

    TokenRefresh()
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

    } catch (e) {
      console.error(error)
    } 
  })
}

export async function ShowDeviceProcess(token: string) {
  await API.get(`/process/${token}`)
  .then(function (response) {
    if (response.data) {
      store.dispatch(setDeviceProcess(response.data))
    }
  })
  .catch(function (error) {
    try {
      const payload = error.response.data.output.payload

      const errorMsg = ''
      + 'error: ' + payload.error + '\n'
      + 'message: ' + payload.message + '\n'
      + 'statusCode: ' + payload.statusCode
      console.error(errorMsg)

    } catch (e) {
      console.error(error)
    } 
  })
}