import Cookies from 'js-cookie'
import axios from "axios";

import { store } from '../redux/store'

const API = () => {
  const accessToken = store.getState().accessToken.value

  console.log(`accessToken: ${accessToken}`)

  return axios.create({
    baseURL: "http://api.alterworld.pp.ua/api/v1/",
    withCredentials: true,
    headers: {
      'Cookie': `refreshToken=${Cookies.get('refreshToken')}; Path=/; HttpOnly;`
    },
  })
}

export default API()
