import { configureStore } from '@reduxjs/toolkit'

import isAuthReducer from './slice/isAuthSlice'
import isDemoReducer from './slice/isDemoSlice'
import accessTokenReducer from './slice/accessTokenSlice'
import errorHandlerReducer from './slice/errorHandlerSlice'
import userNameReducer from './slice/userNameSlice'
import userDeviceReducer from './slice/userDeviceSlice'
import deviceProcessReducer from './slice/deviceProcessSlice'

export const store = configureStore({
  reducer: {
    isAuth: isAuthReducer,
    isDemo: isDemoReducer,
    accessToken: accessTokenReducer,
    errorHandler: errorHandlerReducer,
    userName: userNameReducer,
    userDevice: userDeviceReducer,
    deviceProcess: deviceProcessReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
