import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from "../store"

interface isAuthState {
  value: boolean
}

const initialState: isAuthState = {
  value: false,
}

export const authSlice = createSlice({
  name: 'isAuth',
  initialState,
  reducers: {
    loggedIn: (state) => {
      state.value = true
    },
    loggedOut: (state) => {
      state.value = false
    },
  },
})

export const { loggedIn, loggedOut } = authSlice.actions
export const isAuth = (state: RootState) => state
export default authSlice.reducer
