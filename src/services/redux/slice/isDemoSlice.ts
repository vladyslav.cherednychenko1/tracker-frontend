import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from '../store'

interface isDemoState {
  value: boolean
}

const initialState: isDemoState = {
  value: false,
}

export const demoSlice = createSlice({
  name: 'isDemo',
  initialState,
  reducers: {
    setDemo: (state) => {
      state.value = true
    },
    unsetDemo: (state) => {
      state.value = false
    },
  },
})

export const { setDemo, unsetDemo } = demoSlice.actions
export const isDemo = (state: RootState) => state
export default demoSlice.reducer
