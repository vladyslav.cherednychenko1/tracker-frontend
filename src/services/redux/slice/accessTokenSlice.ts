// import axios from "axios";
// import API from "../../api/apiConfig"

import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from "../store"

interface accessTokenState {
  value: string
}

const initialState: accessTokenState = {
  value: '',
}

export const accessTokenSlice = createSlice({
  name: 'accessToken',
  initialState,
  reducers: {
    setAccessToken: (state, action) => {
      state.value = action.payload
      // API.defaults.headers.common['Authorization'] = `Bearer ${action.payload}`
      // console.log(action.payload)
      // console.log(API.defaults.headers)
      // console.log(API.defaults.headers.common)
    },
  },
})

export const { setAccessToken } = accessTokenSlice.actions
export const accessToken = (state: RootState) => state
export default accessTokenSlice.reducer
