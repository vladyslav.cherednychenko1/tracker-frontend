import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from "../store"

type Token = {
  readonly _id: string;
  readonly computerToken: string;
};

type Device = {
  readonly name: string;
  readonly token: Token;
};

interface userDeviceState {
  value: Array<Device>;
}

const initialState: userDeviceState = {
  value: [],
}

export const userDeviceSlice = createSlice({
  name: 'userDevice',
  initialState,
  reducers: {
    setUserDevice: (state, action) => {
      state.value = action.payload
    },
  },
})

export const { setUserDevice } = userDeviceSlice.actions
export const userDevice = (state: RootState) => state
export default userDeviceSlice.reducer
