import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from "../store"

interface errorHandlerState {
  value: string
}

const initialState: errorHandlerState = {
  value: '',
}

export const errorHandlerSlice = createSlice({
  name: 'errorHandler', 
  initialState,
  reducers: {
    setErrorHandler: (state, action) => {
      state.value = action.payload
    },
  },
})

export const { setErrorHandler } = errorHandlerSlice.actions
export const errorHandler = (state: RootState) => state
export default errorHandlerSlice.reducer
