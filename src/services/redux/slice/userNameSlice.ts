import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from "../store"

interface userNameState {
  value: string
}

const initialState: userNameState = {
  value: '',
}

export const userNameSlice = createSlice({
  name: 'userName',
  initialState,
  reducers: {
    setUserName: (state, action) => {
      state.value = action.payload
    },
  },
})

export const { setUserName } = userNameSlice.actions
export const userName = (state: RootState) => state
export default userNameSlice.reducer
