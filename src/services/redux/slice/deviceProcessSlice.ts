import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from "../store"

type Process = {
  readonly computer: string
  readonly processUUID: string
  readonly processName: string
  readonly processStartTime: string
  readonly processEndTime: string
};

interface processState {
  value: Array<Process>;
}

const initialState: processState = {
  value: [],
}

export const deviceProcessSlice = createSlice({
  name: 'deviceProcess',
  initialState,
  reducers: {
    setDeviceProcess: (state, action) => {
      state.value = action.payload
    },
  },
})

export const { setDeviceProcess } = deviceProcessSlice.actions
export const deviceProcess = (state: RootState) => state
export default deviceProcessSlice.reducer
